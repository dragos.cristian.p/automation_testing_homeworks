import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.Assertions.Calculator;

public class CalculatorTest {

    static Calculator c;

    @BeforeClass
    public static void beforeTest() {
        c = new Calculator();
    }

    @Test
    public void testSum01() {
//        System.out.println(c.compute(2, 7, "+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void testSum02() {
        Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
    }

    @Test
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test
    public void TestDiff01() {
        Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
    }

    @Test
    public void TestProduct01() {
        Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestUsupp01() {
        Assert.assertEquals(121, c.compute(11, 11, "x"), 0);
    }

    @Test
    public void TestDiv01() {
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test
    public void TestDiv02() {
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    // This is the correct way to test if an exception is expedted to be raised
    @Test(expected = IllegalArgumentException.class)
    public void TestDiv03() {
        Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
    }
}


// This is just to catch exception and the test will be passed eitherway, please do not use this example !!
//    @Test
//    public void TestDiv04(){
//        try {
//            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }







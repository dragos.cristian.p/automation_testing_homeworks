package ro.siit.curs4.Homework_3.Draw;

public class Draw {

    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        drawFullShape(Integer.parseInt(args[0]));
    }

    private static void drawShapeCorners(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if (j == 0 || j == height - 1) { // daca suntem pe prima sau ultima linie
                    if (i == 0 || i == width - 1) { //daca suntem pe primul sau ultimul caracter de pe linie
                        System.out.print("*");
                    }
                    else { // daca nu suntem primul sau ultimul caracter
                        System.out.print(" ");
                    }
                }
                else { //daca nu suntem pe prima sau ultima linie
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawShapeCorners(int l) {
        drawShapeCorners(l, l);
    }

    private static void drawShapeOutline(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if (j == 0 || j == height - 1) { // daca suntem pe prima sau ultima linie afisam numai stelute
                    System.out.print("*");
                }
                else { // daca nu suntem pe prima sau ultima linie
                    if (i == 0 || i == width - 1) { //daca suntem pe primul sau ultimul caracter de pe linie
                        System.out.print("*");
                    }
                    else { // daca nu suntem primul sau ultimul caracter
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }

    private static void drawShapeOutline(int l) {
        drawShapeOutline(l,l);
    }

    public static void drawFullShape(int width, int height) {
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void drawFullShape(int l) {
        drawFullShape(l, l);
    }
}

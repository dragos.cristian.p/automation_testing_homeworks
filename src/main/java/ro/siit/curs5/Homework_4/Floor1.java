package ro.siit.curs5.Homework_4;

import java.util.ArrayList;
import java.util.HashMap;

public class Floor1 extends BuildingMain {
    public Floor1(int office, int kitchen, int conferenceRoom, int toilet) {
        super(1, 2, 3, 2);
        Floor1Stuff();
    }

    public static void officeFloor1() {
        HashMap<String, Integer> floor1officeRoom = new HashMap<>();
        floor1officeRoom.put("desks", 20);
        System.out.println("1 Office room");
        System.out.println("Office room 1: " + floor1officeRoom.get("desks") + " desks");
    }

    public static void confFloor1() {
        HashMap<String, Integer> floor1confRoom = new HashMap<>();
        floor1confRoom.put("seats", 10);
        floor1confRoom.put("tv", 1);
        System.out.println("3 Conference rooms");
        System.out.println("Conference room 1: " + floor1confRoom.get("seats") + " seats" + ", " + floor1confRoom.get("tv") + " TV");
        System.out.println("Conference room 2: " + floor1confRoom.get("seats") + " seats" + ", " + floor1confRoom.get("tv") + " TV");
        System.out.println("Conference room 3: " + floor1confRoom.get("seats") + " seats" + ", " + floor1confRoom.get("tv") + " TV");
    }

    public static void kitchenAll() {
        ArrayList<String> kitchenStuff = new ArrayList<>();
        kitchenStuff.add(0, "coffee machine");
        kitchenStuff.add(1, "water dispenser");
        kitchenStuff.add(2, "fridge");
        System.out.println("Kitchen 1: " + kitchenStuff.get(0) + ", " + kitchenStuff.get(1) + ", " + kitchenStuff.get(2));

    }

    public static void toiletsAll() {
        System.out.println("2 Toilets");
        System.out.println("Toilet 1");
        System.out.println("Toilet 2");
    }

    public static void Floor1Stuff() {
        System.out.println(enumFloors.FLOOR1 + ":");
        officeFloor1();
        confFloor1();
        System.out.println("1 Kitchen room");
        kitchenAll();
        toiletsAll();
        System.out.println();
    }
}





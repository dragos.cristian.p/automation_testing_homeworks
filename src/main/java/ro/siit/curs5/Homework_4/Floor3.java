package ro.siit.curs5.Homework_4;

import java.util.ArrayList;
import java.util.HashMap;

public class Floor3 extends BuildingMain {
    public Floor3(int conferenceRoom, int toilet) {
        super(6, 2);
        Floor3Stuff();
    }

    public static void Floor3Stuff() {
        System.out.println(enumFloors.FLOOR3 + ":");
        confFloor3();
        System.out.println("1 Kitchen room");
        kitchenAll();
        toiletsAll();
    }

    public static void confFloor3() {
        HashMap<String, Integer> floor3confRoom = new HashMap<>();
        floor3confRoom.put("seatsConf1", 30);
        floor3confRoom.put("seatsConf2", 20);
        floor3confRoom.put("seatsConf3456", 10);
        floor3confRoom.put("tvConf23456", 1);
        floor3confRoom.put("telepresenceAll", 1);
        floor3confRoom.put("videoProjectorConf1", 1);
        System.out.println("6 Conference rooms");
        System.out.println("Conference room 1: " + floor3confRoom.get("seatsConf1") + " seats" + ", " + floor3confRoom.get("videoProjectorConf1") + " video projector, " + floor3confRoom.get("telepresenceAll") + " telepresence");
        System.out.println("Conference room 2: " + floor3confRoom.get("seatsConf2") + " seats" + ", " + floor3confRoom.get("tvConf23456") + " TV, " + floor3confRoom.get("telepresenceAll") + " telepresence");
        System.out.println("Conference room 3: " + floor3confRoom.get("seatsConf3456") + " seats" + ", " + floor3confRoom.get("tvConf23456") + " TV, " + floor3confRoom.get("telepresenceAll") + " telepresence");
        System.out.println("Conference room 4: " + floor3confRoom.get("seatsConf3456") + " seats" + ", " + floor3confRoom.get("tvConf23456") + " TV, " + floor3confRoom.get("telepresenceAll") + " telepresence");
        System.out.println("Conference room 5: " + floor3confRoom.get("seatsConf3456") + " seats" + ", " + floor3confRoom.get("tvConf23456") + " TV, " + floor3confRoom.get("telepresenceAll") + " telepresence");
        System.out.println("Conference room 6: " + floor3confRoom.get("seatsConf3456") + " seats" + ", " + floor3confRoom.get("tvConf23456") + " TV, " + floor3confRoom.get("telepresenceAll") + " telepresence");

    }

    public static void kitchenAll() {
        ArrayList<String> kitchenStuff = new ArrayList<>();
        kitchenStuff.add(0, "coffee machine");
        kitchenStuff.add(1, "water dispenser");
        kitchenStuff.add(2, "fridge");
        System.out.println("Kitchen 1: " + kitchenStuff.get(0) + ", " + kitchenStuff.get(1) + ", " + kitchenStuff.get(2));

    }

    public static void toiletsAll() {
        System.out.println("2 Toilets");
        System.out.println("Toilet 1");
        System.out.println("Toilet 2");
    }
}

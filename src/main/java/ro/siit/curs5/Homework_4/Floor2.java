package ro.siit.curs5.Homework_4;

import java.util.ArrayList;
import java.util.HashMap;

public class Floor2 extends BuildingMain {
    public Floor2(int office, int kitchen, int conferenceRoom, int toilet) {
        super(2, 1, 4, 2);
        Floor2Stuff();
    }


    public static void officeFloor2() {
        HashMap<String, Integer> floor2officeRoom = new HashMap<>();
        floor2officeRoom.put("desks", 10);
        System.out.println("2 Office rooms");
        System.out.println("Office room 1: " + floor2officeRoom.get("desks") + " desks");
        System.out.println("Office room 2: " + floor2officeRoom.get("desks") + " desks");
    }


    public static void confFloor2() {
        HashMap<String, Integer> floor2confRoom = new HashMap<>();
        floor2confRoom.put("seats", 8);
        floor2confRoom.put("tv", 1);
        System.out.println("4 Conference rooms");
        System.out.println("Conference room 1: " + floor2confRoom.get("seats") + " seats" + ", " + floor2confRoom.get("tv") + " TV");
        System.out.println("Conference room 2: " + floor2confRoom.get("seats") + " seats" + ", " + floor2confRoom.get("tv") + " TV");
        System.out.println("Conference room 3: " + floor2confRoom.get("seats") + " seats" + ", " + floor2confRoom.get("tv") + " TV");
        System.out.println("Conference room 4: " + floor2confRoom.get("seats") + " seats" + ", " + floor2confRoom.get("tv") + " TV");
    }

    public static void kitchenAll() {
        ArrayList<String> kitchenStuff = new ArrayList<>();
        kitchenStuff.add(0, "coffee machine");
        kitchenStuff.add(1, "water dispenser");
        kitchenStuff.add(2, "fridge");
        System.out.println("Kitchen 1: " + kitchenStuff.get(0) + ", " + kitchenStuff.get(1) + ", " + kitchenStuff.get(2));

    }

    public static void toiletsAll() {
        System.out.println("2 Toilets");
        System.out.println("Toilet 1");
        System.out.println("Toilet 2");
    }

    public static void Floor2Stuff() {
        System.out.println(enumFloors.FLOOR2 + ":");
        officeFloor2();
        confFloor2();
        System.out.println("1 Kitchen room");
        kitchenAll();
        toiletsAll();
        System.out.println();
    }

}



